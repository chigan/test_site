from django.db import models
import datetime
# Create your models here.

class temperature(models.Model):
    date = models.DateField(default=datetime.date.today)
    value = models.IntegerField(default=0)