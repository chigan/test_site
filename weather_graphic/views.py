from weather_graphic.models import temperature
import datetime
from django.views.generic.list import ListView
import requests
from xml.etree import ElementTree
# Create your views here.

class TemperatureListView(ListView):
     model = temperature
     context_object_name = 'temperature_for_30_days'
     
     @staticmethod
     def get_now_temperature():
          response = requests.get("http://export.yandex.ru/weather-ng/forecasts/27612.xml")
          root = ElementTree.fromstring(response.content)
          temperature = int(root[0][4].text)
          date_str = root[0][3].text
          date = datetime.date(int(date_str[:4]),int(date_str[5:7]),int(date_str[8:10]))
          return {'date' : date, 'temperature': temperature}
            
     def get_queryset(self, **kwargs):
          d_set = temperature.objects.all().reverse()[30:]
          if list(d_set)[-1].date != datetime.date.today():
               now_temperature = self.get_now_temperature()
               temperature.objects.create(date=now_temperature['date'], value=now_temperature['temperature'])
               d_set = temperature.objects.all().reverse()[30:]
          return d_set