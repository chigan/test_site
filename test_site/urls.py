from django.conf.urls import patterns, include, url
from django.contrib import admin
from weather_graphic.views import TemperatureListView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_site.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemperatureListView.as_view())
)
